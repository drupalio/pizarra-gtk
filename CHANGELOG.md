# Cambios

## 1.0.1

- Resuelto un bug que impedía abrir un archivo en la segunda instancia abierta
  de la pizarra.

## 1.0.0

- Primera versión estable
- Se pueden hacer líneas, círculos perfectos, elipses, polígonos y rectángulos
- Se pueden borrar figuras
- Funciona el zoom
- Se pueden abrir y guardar archivos
- Se puede exportar a PNG
- Se puede elegir color de una paleta
- Se puede deshacer/rehacer
